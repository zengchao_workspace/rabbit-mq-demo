package com.buydeem.share.rabbit;

import com.rabbitmq.client.ConnectionFactory;

/**
 *
 * @author zengchao
 * @date 2021-05-11 12:11:11
 */
public class Utils {
    private static final ConnectionFactory FACTORY;

    static {
        FACTORY = new ConnectionFactory();
        FACTORY.setUsername("root");
        FACTORY.setPassword("root");
        FACTORY.setVirtualHost("test");
        FACTORY.setHost("127.0.0.1");
        FACTORY.setPort(5672);
    }

    public static ConnectionFactory getFactory(){
        return FACTORY;
    }
}
