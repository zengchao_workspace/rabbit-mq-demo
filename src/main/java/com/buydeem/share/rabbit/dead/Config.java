package com.buydeem.share.rabbit.dead;

/**
 * todo 描述信息
 *
 * @author zengchao
 * @date 2021-05-11 12:07:07
 */
public class Config {
    /**
     * 业务Exchange
     */
    public static final String BUSINESS_EXCHANGE = "order.direct";
    /**
     * 业务Queue
     */
    public static final String BUSINESS_QUEUE = "order.queue";
    /**
     * 业务Key
     */
    public static final String BUSINESS_ROUTING_KEY = "order";
    /**
     * 死信Exchange
     */
    public static final String DEAD_EXCHANGE = "dead.direct";
    /**
     * 死信Queue
     */
    public static final String DEAD_QUEUE = "dead.queue";
    /**
     * 死信Key
     */
    public static final String DEAD_ROUTING_KEY = "dead";
}
