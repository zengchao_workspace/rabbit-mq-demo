package com.buydeem.share.rabbit.dead;

import com.buydeem.share.rabbit.Utils;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

/**
 * 生成者
 *
 * @author zengchao
 * @date 2021-05-11 14:37:37
 */
@Slf4j
public class ProductApp {
    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory factory = Utils.getFactory();
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        byte[] msg1 = "ok".getBytes(StandardCharsets.UTF_8);
        channel.basicPublish(Config.BUSINESS_EXCHANGE,Config.BUSINESS_ROUTING_KEY,new AMQP.BasicProperties(),msg1);

        for (int i = 0; i < 5; i++) {
            byte[] msg = String.format("死信消息[%d]",i).getBytes(StandardCharsets.UTF_8);
            channel.basicPublish(Config.BUSINESS_EXCHANGE,Config.BUSINESS_ROUTING_KEY,new AMQP.BasicProperties(),msg);
        }

        log.info("消息全部发送完成!");

        channel.close();
        connection.close();
    }
}
