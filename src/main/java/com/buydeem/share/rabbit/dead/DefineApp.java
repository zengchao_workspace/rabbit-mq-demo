package com.buydeem.share.rabbit.dead;

import com.buydeem.share.rabbit.Utils;
import com.rabbitmq.client.*;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;


/**
 * 声明Exchange和Queue
 * @author zengchao
 * @date 2021-05-11 11:16:16
 */
@Slf4j
public class DefineApp {

    public static void main(String[] args) throws IOException, TimeoutException {
        Connection connection = Utils.getFactory().newConnection();
        Channel channel = connection.createChannel();

        /**
         * 正常的业务Exchange和Queue
         */
        channel.exchangeDeclare(Config.BUSINESS_EXCHANGE, BuiltinExchangeType.DIRECT, true);
        Map<String, Object> arg = new HashMap<>();
        arg.put("x-dead-letter-exchange", Config.DEAD_EXCHANGE);
        arg.put("x-dead-letter-routing-key", Config.DEAD_ROUTING_KEY);
        channel.queueDeclare(Config.BUSINESS_QUEUE, true, false, false, arg);
        channel.queueBind(Config.BUSINESS_QUEUE,Config.BUSINESS_EXCHANGE,Config.BUSINESS_ROUTING_KEY);

        /**
         * 死信Exchange和Queue
         */
        channel.exchangeDeclare(Config.DEAD_EXCHANGE, BuiltinExchangeType.DIRECT,true);
        channel.queueDeclare(Config.DEAD_QUEUE,true,false,false,new HashMap<>());
        channel.queueBind(Config.DEAD_QUEUE,Config.DEAD_EXCHANGE,Config.DEAD_ROUTING_KEY);

        channel.close();
        connection.close();
    }

}
