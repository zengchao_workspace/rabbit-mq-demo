package com.buydeem.share.rabbit.delay;

/**
 *
 * @author zengchao
 * @date 2021-05-12 09:18:18
 */
public class Config {

    public static String ORDER_EXCHANGE = "order.exchange";
    public static String ORDER_ROUTING_KEY = "order";
    public static String ORDER_QUEUE = "order.queue";

    public static String DELAYED_EXCHANGE = "delayed.exchange";
    public static String DELAYED_ROUTING_KEY = "delayed";
    public static String DELAYED_QUEUE = "delayed.queue";


    public static String PLUGINS_EXCHANGE = "plugins.exchange";
    public static String PLUGINS_ROUTING_KEY = "plugins";
    public static String PLUGINS_QUEUE = "plugins.queue";

}
