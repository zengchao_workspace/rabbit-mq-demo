package com.buydeem.share.rabbit.delay;

import com.buydeem.share.rabbit.Utils;
import com.rabbitmq.client.*;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeoutException;

/**
 * 消费者
 *
 * @author zengchao
 * @date 2021-05-12 09:55:55
 */
@Slf4j
public class ConsumerApp {
    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory factory = Utils.getFactory();
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        //业务队列消息消费
        channel.basicConsume(Config.DELAYED_QUEUE, true, new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String content = sdf.format(new Date());

                String msg = new String(body, StandardCharsets.UTF_8);

                log.info("消费者收到消息:{},当前时间:{}", msg, content);
            }
        });
    }
}
