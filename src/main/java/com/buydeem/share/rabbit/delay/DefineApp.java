package com.buydeem.share.rabbit.delay;

import com.buydeem.share.rabbit.Utils;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

/**
 * 定义Exchange和队列等信息
 *
 * @author zengchao
 * @date 2021-05-12 09:24:24
 */
public class DefineApp {
    public static void main(String[] args) throws IOException, TimeoutException {
        Connection connection = Utils.getFactory().newConnection();
        Channel channel = connection.createChannel();

        /**
         * 正常的业务Exchange和Queue
         */
        channel.exchangeDeclare(Config.ORDER_EXCHANGE, BuiltinExchangeType.DIRECT, true);
        Map<String, Object> arg = new HashMap<>();
        arg.put("x-dead-letter-exchange", Config.DELAYED_EXCHANGE);
        arg.put("x-dead-letter-routing-key", Config.DELAYED_ROUTING_KEY);
        arg.put("x-message-ttl",10000);
        channel.queueDeclare(Config.ORDER_QUEUE, true, false, false, arg);
        channel.queueBind(Config.ORDER_QUEUE,Config.ORDER_EXCHANGE,Config.ORDER_ROUTING_KEY);
        /**
         * 死信Exchange和Queue
         */
        channel.exchangeDeclare(Config.DELAYED_EXCHANGE, BuiltinExchangeType.DIRECT,true);
        channel.queueDeclare(Config.DELAYED_QUEUE,true,false,false,new HashMap<>());
        channel.queueBind(Config.DELAYED_QUEUE,Config.DELAYED_EXCHANGE,Config.DELAYED_ROUTING_KEY);

        channel.close();
        connection.close();
    }
}
