package com.buydeem.share.rabbit.delay;

import com.buydeem.share.rabbit.Utils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

/**
 * 定义延迟队列等信息
 *
 * @author zengchao
 * @date 2021-05-12 13:35:35
 */
public class PluginsDefineApp {
    public static void main(String[] args) throws IOException, TimeoutException {
        Connection connection = Utils.getFactory().newConnection();
        Channel channel = connection.createChannel();

        Map<String,Object> argMap = new HashMap<>();
        argMap.put("x-delayed-type","direct");
        channel.exchangeDeclare(Config.PLUGINS_EXCHANGE,"x-delayed-message",true,false,argMap);
        channel.queueDeclare(Config.PLUGINS_QUEUE,true,false,false,new HashMap<>());
        channel.queueBind(Config.PLUGINS_QUEUE,Config.PLUGINS_EXCHANGE,Config.PLUGINS_ROUTING_KEY);

        channel.close();
        connection.close();
    }
}
