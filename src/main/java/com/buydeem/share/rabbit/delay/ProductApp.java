package com.buydeem.share.rabbit.delay;

import com.buydeem.share.rabbit.Utils;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.concurrent.TimeoutException;

/**
 * 生产者
 *
 * @author zengchao
 * @date 2021-05-12 09:52:52
 */
@Slf4j
public class ProductApp {

    public static void main(String[] args) throws IOException, TimeoutException {
        //发送正常消息
        //send();
        //发送消息设置了ttl的
        //send2();
        send3(Arrays.asList(5,3));
    }

    private static void send() throws IOException, TimeoutException {
        ConnectionFactory factory = Utils.getFactory();
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String content = sdf.format(new Date());
        log.info("生产者发送消息:{}", content);
        byte[] msg = content.getBytes(StandardCharsets.UTF_8);
        channel.basicPublish(Config.ORDER_EXCHANGE, Config.ORDER_ROUTING_KEY, new AMQP.BasicProperties(), msg);

        channel.close();
        connection.close();
    }

    /**
     * 单条发送延时
     * @throws IOException
     * @throws TimeoutException
     */
    private static void send2() throws IOException, TimeoutException {
        ConnectionFactory factory = Utils.getFactory();
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String content = sdf.format(new Date());
        log.info("生产者发送消息:{}", content);
        byte[] msg = content.getBytes(StandardCharsets.UTF_8);
        AMQP.BasicProperties properties = new AMQP.BasicProperties.Builder().expiration("5000").build();
        channel.basicPublish(Config.ORDER_EXCHANGE, Config.ORDER_ROUTING_KEY, properties, msg);

        channel.close();
        connection.close();
    }

    /**
     * 多条发送延时
     * @throws IOException
     * @throws TimeoutException
     */
    private static void send3(Collection<Integer> delayedTimes) throws IOException, TimeoutException {
        ConnectionFactory factory = Utils.getFactory();
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for (Integer delayedTime : delayedTimes) {
            String content = String.format("消息时间:[%s],延时[%d]s", sdf.format(new Date()), delayedTime);
            log.info(content);
            byte[] msg = content.getBytes(StandardCharsets.UTF_8);
            AMQP.BasicProperties properties = new AMQP.BasicProperties.Builder().expiration(String.valueOf(delayedTime * 1000)).build();
            channel.basicPublish(Config.ORDER_EXCHANGE, Config.ORDER_ROUTING_KEY, properties, msg);
        }

        channel.close();
        connection.close();
    }

    private static void send4() throws IOException, TimeoutException {
        ConnectionFactory factory = Utils.getFactory();
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String content = sdf.format(new Date());
        log.info("生产者发送消息:{}", content);
        byte[] msg = content.getBytes(StandardCharsets.UTF_8);
        AMQP.BasicProperties properties = new AMQP.BasicProperties.Builder().expiration("5000").build();
        channel.basicPublish(Config.ORDER_EXCHANGE, Config.ORDER_ROUTING_KEY, properties, msg);

    }
}
